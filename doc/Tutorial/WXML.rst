WXML Library
============

While in theory it is a fairly simply task to write XML in any
language, in practice it’s not quite that easy.  Balancing tags that
are opened at one end of a 2000 line source file and closed at the
other or even opened and closed in different source files, is an
unnecessary memory exercise, and balancing them in the right order a
pain. More importantly, you’ll probably only spot your mistake when
you try to process the file, and given that XML is case sensitive it’s
a pretty easy to make a mistake. Add to this the fact that it is
extremely difficult to correctly indent an XML document whose
structure is as dynamic as that of the underlying program (just using
write statements). Hence the creation of a library for handling the
more tedious or tricky elements of creating a XML document. We also
added functionality to check the well-formedness of all XML.


Routines
--------

General routines
~~~~~~~~~~~~~~~~

1. `xml_OpenFile` - Mandatory Initialization routine
2. `xml_Close` - Mandatory finalization routine, closes
   channels, etc
3. `str <#str>` - utility to convert reals and integers to character
   strings

XML routines
~~~~~~~~~~~~

1. `xml_NewElement <#start>` - writes an xml start tag
2. `xml_AddAttribute <#attr>` - adds an attribute to a tag
3. `xml_AddPcdata <#text>` - adds text to an xml element
4. `xml_AddArray <#array>` - dumps the contents of an array as pcdata
5. `xml_EndElement <#end>` - writes an xml end tag

Subroutine Guide

1. .. rubric:: `xml_OpenFile(filename, ind, xf)`
      :name: xml_openfilefilename-ind-xf

   +----------+--------------------------------+------------------+----------+---------+
   | argument | role                           | type             | optional | default |
   +==========+================================+==================+==========+=========+
   | filename | xml filename                   | character(len=*) | no       |         |
   +----------+--------------------------------+------------------+----------+---------+
   | ind      | controls indentation of output | logical          | yes      | .true.  |
   +----------+--------------------------------+------------------+----------+---------+
   | xf       | xml filename                   | type(xmlf_t)     | no       |         |
   +----------+--------------------------------+------------------+----------+---------+

2. .. rubric:: `xml_Close(filehandle) <>`
      :name: xml_closefilehandle

   ======== ============== ============ ======== =======
   argument role           type         optional default
   ======== ============== ============ ======== =======
   xf       xml filehandle type(xmlf_t) no        
   ======== ============== ============ ======== =======

3. .. rubric:: `function str(value, format) <>`
      :name: function-strvalue-format

   +----------+------------------+------------------+----------+---------+
   | argument | role             | type             | optional | default |
   +==========+==================+==================+==========+=========+
   | value    | value to convert | real*8, real*4,  | no       |         |
   |          | to string        | integer, logical |          |         |
   +----------+------------------+------------------+----------+---------+
   | format   | format for reals | character(len=*) | yes      | g22.12  |
   +----------+------------------+------------------+----------+---------+


1. .. rubric:: `xml_NewElement(xf, name) <>`
      :name: xml_newelementxf-name

   ======== ================== ================ ======== =======
   argument role               type             optional default
   ======== ================== ================ ======== =======
   xf       xml filehandle     type(xmlf_t)     no        
   name     name of tag to add character(len=*) no        
   ======== ================== ================ ======== =======

2. .. rubric:: `xml_AddAttribute(xf, attname, value) <>`
      :name: xml_addattributexf-attname-value

   +----------+-----------------+------------------+----------+---------+
   | argument | role            | type             | optional | default |
   +==========+=================+==================+==========+=========+
   | xf       | xml filehandle  | type(xmlf_t)     | no       |         |
   +----------+-----------------+------------------+----------+---------+
   | attname  | attribute name  | character(len=*) | no       |         |
   +----------+-----------------+------------------+----------+---------+
   | value    | attribute value | character(len=*) | no       |         |
   |          |                 | (convert using   |          |         |
   |          |                 | str())           |          |         |
   +----------+-----------------+------------------+----------+---------+

3. .. rubric:: `xml_AddPcdata(xf, pcdata) <>`
      :name: xml_addpcdataxf-pcdata

   +----------+----------------+------------------+----------+---------+
   | argument | role           | type             | optional | default |
   +==========+================+==================+==========+=========+
   | xf       | xml filehandle | type(xmlf_t)     | no       |         |
   +----------+----------------+------------------+----------+---------+
   | pcdata   | string to add  | character(len=*) | no       |         |
   |          |                | (convert numbers |          |         |
   |          |                | using str())     |          |         |
   +----------+----------------+------------------+----------+---------+

4. .. rubric:: `xml_AddArray(xf, a, format) <>`
      :name: xml_addarrayxf-a-format

   +----------+----------------+-----------------------+----------+---------------------+
   | argument | role           | type                  | optional | default             |
   +==========+================+=======================+==========+=====================+
   | xf       | xml filehandle | type(xmlf_t)          | no       |                     |
   +----------+----------------+-----------------------+----------+---------------------+
   | a        | array (:)      | integer, real, double | no       |                     |
   +----------+----------------+-----------------------+----------+---------------------+
   | format   | format         | character(len=*)      | yes      | 6(i12) / 4(es20.12) |
   +----------+----------------+-----------------------+----------+---------------------+

5. .. rubric:: `xml_EndElement(xf, name) <>`
      :name: xml_endelementxf-name

   ======== ======================== ================ ======== =======
   argument role                     type             optional default
   ======== ======================== ================ ======== =======
   xf       xml filehandle           type(xmlf_t)     no        
   name     name of element to close character(len=*) no        
   ======== ======================== ================ ======== =======


