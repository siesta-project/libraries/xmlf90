program xmlcheck
!
! Checks for well-formedness of an XML file
!
  use xmlf90_sax
  use iso_fortran_env, only: output_unit, error_unit

  integer :: iostat
  type(xml_t)  :: fxml
  character(len=256) :: filename

  CALL GET_COMMAND_ARGUMENT(1, filename,STATUS=iostat)
  if (iostat /= 0) stop "Cannot get file name from command line"

  call open_xmlfile(trim(filename),fxml,iostat)
  if (iostat /= 0) then
     write(error_unit,"(a)") "Cannot open file " // trim(filename)
     STOP
  endif

  call xml_parse(fxml, verbose = .false.)

  print *, "Characters processed: ", xml_char_count(fxml)

end program xmlcheck














